import React from "react";
import { View, TextInput, StyleSheet } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

const SearchBar = ({ searchString, onSearchStringChange, onStringSubit }) => {
  return (
    <View style={styles.searchBarBackground}>
      <FontAwesome name="search" style={styles.searchIconStyle} />
      <TextInput
        style={styles.inputStyle}
        autoCorrect={false}
        autoCapitalize="none"
        placeholder="Search"
        value={searchString}
        onChangeText={(value) => {
          onSearchStringChange(value);
        }}
        onEndEditing={() => onStringSubit()} // init a search
      />
    </View>
  );
};

const styles = StyleSheet.create({
  searchBarBackground: {
    backgroundColor: "#F0EEEE",
    height: 50,
    borderRadius: 5,
    margin: 10,
    flexDirection: "row",
    marginBottom: 10,
  },
  inputStyle: {
    flex: 1,
    paddingLeft: 16,
    fontSize: 18,
  },
  searchIconStyle: {
    fontSize: 35,
    alignSelf: "center",
    paddingLeft: 15,
  },
});

export default SearchBar;
