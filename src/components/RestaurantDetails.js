import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";

const RestaurantDetails = ({ result, navigate, itemId }) => {
  return (
    <View style={styles.restaurantView}>
      <TouchableOpacity
        onPress={() => {
          navigate("Restaurant", { id: itemId });
        }}
      >
        <Image style={styles.image} source={{ uri: result.image_url }} />
        <Text style={styles.name}>{result.name}</Text>
        <Text style={styles.description}>
          {result.rating} Stars, {result.review_count} Reviews
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  restaurantView: {
    marginLeft: 10,
    marginRight: 10,
  },
  image: {
    width: 250,
    height: 120,
    borderRadius: 4,
    marginBottom: 5,
  },
  name: {
    fontWeight: "bold",
    fontSize: 15,
  },
  description: {
    color: "#8a8a8a65",
  },
});

export default RestaurantDetails;
