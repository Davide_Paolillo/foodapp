import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { withNavigation } from 'react-navigation'
import RestaurantDetails from "./RestaurantDetails";

const ResultList = ({ title, results, navigation }) => {
  return (
    <View>
      <Text style={styles.title}>{title}</Text>
      <FlatList
        style={styles.container}
        horizontal
        data={results}
        keyExtractor={(result) => result.id}
        renderItem={({ item }) => {
          return (
          <RestaurantDetails 
            result={item}
            navigate={navigation.navigate}
            itemId={item.id}
          />);
        }}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    fontWeight: "bold",
    marginLeft: 10,
  },
  container: {
      marginBottom: 10,
      marginTop: 5,
  },
});

export default withNavigation(ResultList);
