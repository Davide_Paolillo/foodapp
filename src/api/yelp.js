import axios from "axios";

export default axios.create({
  // prevents us to writhe this string when we will call funtions like yelp.get('/search')
  // where yelp is the class and get is the query, this is equals to yelp.get('https://api.yelp.com/v3/businesses/search')
  baseURL: "https://api.yelp.com/v3/businesses",
  headers: {
      Authorization: 'Bearer U0VghPRT5dms79YP3IjSDqgnACVUh4M6RerT0rVtTBVBK5SnpIvijRBx6WS3WsGPWU5EkhB6QJZyf_UxLm_4efrVCT1eK2VZtm56jk3ZQhA5eXNJsE2jSIhVJ9wOX3Yx'
  }
});
