import React, { useState, useEffect } from "react";
import { Text, StyleSheet, FlatList, Image } from "react-native";
import RestaurantDetails from "../components/RestaurantDetails";
import yelp from "../api/yelp";

const RestaurantScreen = ({ navigation }) => {
    const [result, setResult] = useState(null);
    const id = navigation.getParam('id');

    const getResult = async (id) => {
        const response = await yelp.get(`/${id}`);
        setResult(response.data);
    };

    useEffect(() => {
        getResult(id);
    }, []);

    if(!result) { // means if result is null
        return null;
    } else {
        return (
        <>
            <Text>{result.name}</Text>
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={result.photos}
                keyExtractor={(photo) => photo}
                renderItem={({ item }) => {
                    return <Image style={styles.image} source={{ uri: item }}/>
                }}
            />
        </>);
    }
};

const styles = StyleSheet.create({
    image: {
        width: 250,
        height:250,
        marginHorizontal: 1,
    },
});

export default RestaurantScreen;