import React, { useState } from "react";
import { View, ScrollView, Text, StyleSheet } from "react-native";
import SearchBar from "../components/SearchBar";
import useRestaurants from "../hooks/useRestaurants";
import ResultList from "../components/ResultsList";

const SearchScreen = () => {
  const [searchString, setString] = useState("");
  const [searchApi, results, errorMessage] = useRestaurants();

  const filterResultsByPrice = (price) => {
    return results.filter((result) => {
      return result.price == price;
    });
  };

  return (
    <>
      <SearchBar
        searchString={searchString}
        onSearchStringChange={(newString) => {
          setString(newString);
        }}
        onStringSubit={() => {
          searchApi(searchString);
          setString("");
        }}
      />
      {errorMessage.length > 0 ? (
        <Text style={styles.text}>{errorMessage}</Text>
      ) : null}
      <ScrollView>
        {filterResultsByPrice("$").length < 1 ? null : (
          <ResultList
            title="Cost Effective"
            results={filterResultsByPrice("$")}
          />
        )}
        {filterResultsByPrice("$$").length < 1 ? null : (
          <ResultList 
            title="Bit Pricer" 
            results={filterResultsByPrice("$$")}
          />
        )}
        {filterResultsByPrice("$$$").length < 1 ? null : (
          <ResultList
            title="Big Spender!"
            results={filterResultsByPrice("$$$")}
          />
        )}
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    color: "red",
    marginLeft: 10,
  },
  findText: {
    marginLeft: 10,
  },
  container: {
    flex: 1,
  },
});

export default SearchScreen;
